FROM alpine:latest

ENV S6_OVERLAY_VERSION v1.21.4.0

# Terminate all s6 childs when init stage fails
ENV S6_BEHAVIOUR_IF_STAGE2_FAILS 2

ENV FILEBIN_VERSION 3.4.4
ENV FILEBIN_HOME /var/www/filebin

# Install main packages and build-dependencies and do some other stuff
RUN apk update \
    && apk add --update --no-cache --no-progress bash curl imagemagick nginx msmtp php7 php7-ctype php7-exif php7-fileinfo php7-fpm php7-json php7-gd php7-mbstring php7-mysqli php7-phar php7-pecl-apcu php7-session py3-pygments \
    && apk add --update --no-cache --no-progress --virtual .build-deps composer git nodejs \
    && wget https://github.com/just-containers/s6-overlay/releases/download/${S6_OVERLAY_VERSION}/s6-overlay-amd64.tar.gz \
    && tar -C / -xzvf s6-overlay-amd64.tar.gz \
    && rm s6-overlay-amd64.tar.gz \
    && ln -s /usr/bin/python3 /usr/bin/python \
    && cd /tmp && wget https://files.pythonhosted.org/packages/b7/f5/0d658908d70cb902609fbb39b9ce891b99e060fa06e98071d369056e346f/ansi2html-1.5.2.tar.gz \
    && tar -xzvf ansi2html-1.5.2.tar.gz && rm ansi2html-1.5.2.tar.gz && cd ansi2html-1.5.2 \
    && python3 setup.py build && python3 setup.py install --prefix=/usr \
    && cd / && rm -r /tmp/ansi2html-1.5.2 \
    && git clone --branch ${FILEBIN_VERSION} --depth 1 https://git.server-speed.net/users/flo/filebin "$FILEBIN_HOME" \
    && php /var/www/filebin/check_deps.php \
    && git -C "${FILEBIN_HOME}" submodule update --init --recursive \
    && composer --working-dir="${FILEBIN_HOME}/" install --no-dev \
    && chown -R nginx:nginx "$FILEBIN_HOME" \
    && rm -rf "${FILEBIN_HOME}/.git" \
    && mkdir -p /run/php-fpm/ /run/nginx/ \
    && ln -sf /dev/stdout /var/log/nginx/access.log \
    && ln -sf /dev/stderr /var/log/nginx/error.log \
    && chown nginx:nginx /run/php-fpm/ /run/nginx/ \
    && sed -E -e 's/^user = nobody$/user = nginx/' \
              -e 's/^group = nobody$/user = nginx/' \
              -e 's|^listen = 127.0.0.1:9000$|listen = /run/php-fpm/php-fpm.sock|' \
              -e 's/^;listen.owner = nobody$/listen.owner = nginx/' \
              -e 's/^;listen.group = nobody$/listen.owner = nginx/' \
              -e 's/^;(listen.mode = 0660)$/\1/' \
              -i /etc/php7/php-fpm.d/www.conf \
    && bash -c "${FILEBIN_HOME}/scripts/optimize_js.sh" \
    && apk del --purge .build-deps

# Copy the configuration file of nginx
COPY ./config/default.conf /etc/nginx/conf.d/default.conf

# Copy required assets for s6-overlay
COPY ./assets/s6-overlay/ /etc/

ENTRYPOINT ["/init"]
EXPOSE 80
HEALTHCHECK --interval=30s --retries=3 CMD curl --fail http://localhost:80 || exit 1
