### After startup
Don't forget to add users after container startup with:
```
docker exec -it <container id or name> php /var/www/filebin/index.php user add_user
```
